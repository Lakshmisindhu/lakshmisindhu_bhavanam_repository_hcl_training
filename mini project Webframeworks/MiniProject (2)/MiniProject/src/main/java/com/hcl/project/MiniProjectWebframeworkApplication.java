package com.hcl.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniProjectWebframeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniProjectWebframeworkApplication.class, args);
	}

}
