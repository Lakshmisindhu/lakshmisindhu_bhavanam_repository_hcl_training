package com.hcl.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.project.entities.*;




public interface UserRepo extends JpaRepository<User, Integer> {

	User findByNameAndPassword(String name, String password);
}
